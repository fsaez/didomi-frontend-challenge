/**
 * Created by felipesaez on 6/04/18.
 */
import React from 'react';
import ConsentsTable from 'components/consentsTable';

const ConsentManagementPage = () => (
  <ConsentsTable />
);

export default ConsentManagementPage;