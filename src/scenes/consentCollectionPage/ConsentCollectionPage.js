/**
 * Created by felipesaez on 5/04/18.
 */
import React from 'react';
import ConsentCollectionForm from 'components/consentCollectionForm';


const ConsentCollectionPage = () => (
  <ConsentCollectionForm/>
);

export default ConsentCollectionPage;