/**
 * Created by felipesaez on 6/04/18.
 */
import React from 'react';
import MaterialUiCheckbox from 'material-ui/Checkbox';

const Checkbox = (props) => (
  <div>
    <MaterialUiCheckbox
      label={ props.text }
      checked={ props.checked }
      id={ props.id }
      onCheck={ e => props.onChange(props.index) } />
  </div>
);

export default Checkbox;