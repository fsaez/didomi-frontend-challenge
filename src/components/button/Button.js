/**
 * Created by felipesaez on 6/04/18.
 */
import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const Button = (props) => (
  <RaisedButton
    id={ props.id }
    style={ props.style }
    disabled={ props.disabled }
    onClick={ props.onClick }>{ props.children }</RaisedButton>
);

export default Button;