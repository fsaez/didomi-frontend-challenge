/**
 * Created by felipesaez on 5/04/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import ConsentCollectionForm from './ConsentCollectionForm';
import { updateName, updateEmailAddress, submitForm, resetForm } from 'actions/actions';
import { getSelectedItemsSelector } from 'reducers/selectors';

class ConsentCollectionFormContainer extends React.Component {
  componentWillUnmount() {
    this.props.resetForm();
  }
  render() {
    if (this.props.success) {
      return <Redirect to="/consents"/>
    }
    return <ConsentCollectionForm
      currentName={ this.props.currentName }
      currentEmailAddress={ this.props.currentEmailAddress }
      onNameChange={ this.props.updateName }
      onEmailAddressChange={ this.props.updateEmailAddress }
      submitForm={ this.props.submitForm }
      consentsSelected={ this.props.consentsSelected }
      isFormValid={ this.props.isFormValid }/>;
  }
}

const mapStateToProps = (state) => ({
  currentName: state.form.name,
  currentEmailAddress: state.form.email,
  isFormValid: state.form.isValid,
  success: state.form.success,
  consentsSelected: getSelectedItemsSelector(state)
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({ updateName, updateEmailAddress, submitForm, resetForm }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ConsentCollectionFormContainer);