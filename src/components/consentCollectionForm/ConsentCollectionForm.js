/**
 * Created by felipesaez on 5/04/18.
 */
import React from 'react';
import TextField from 'components/textField'
import Button from 'components/button';
import ConsentsList from 'components/consentsList';
import { textFieldInlineStyle, submitButtonInlineStyle, Wrapper, Text } from './styles';

const ConsentCollectionForm = (props) => (
  <Wrapper>
    <TextField
      style={ textFieldInlineStyle }
      placeholder="Enter your name"
      value={ props.currentName.text }
      onChange={ props.onNameChange }/>

    <TextField
      errorText={ props.currentEmailAddress.errorText }
      placeholder="Enter your email address"
      value={ props.currentEmailAddress.text }
      onChange={ props.onEmailAddressChange }/>

    <Text>I agree to:</Text>

    <ConsentsList />

    <Button
      style={ submitButtonInlineStyle }
      disabled={ !props.isFormValid }
      onClick={ e => props.submitForm(props.isFormValid, {
        name: props.currentName.text,
        email: props.currentEmailAddress.text,
        consents: props.consentsSelected.map(consent => consent.text)
    }) }>Submit</Button>
  </Wrapper>
);

export default ConsentCollectionForm;