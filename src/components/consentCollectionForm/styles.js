/**
 * Created by felipesaez on 9/04/18.
 */
import styled from 'styled-components';

export const Wrapper = styled.div`
  margin: auto;
  margin-top: 40px;
  width: 600px;
`;

export const Text = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
`;

//we override the default value
export const textFieldInlineStyle = {
  display: 'block'
};

//we override the default value
export const submitButtonInlineStyle = {
  marginTop: 20
};