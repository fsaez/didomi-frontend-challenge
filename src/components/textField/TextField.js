/**
 * Created by felipesaez on 5/04/18.
 */
import React from 'react';
import MaterialUiTextField from 'material-ui/TextField';

const TextField = (props) => (
  <MaterialUiTextField
    hintText={ props.placeholder }
    errorText={ props.errorText }
    value={ props.value }
    style={ props.style }
    onChange={ e => props.onChange(e.target.value) }/>
);

export default TextField;