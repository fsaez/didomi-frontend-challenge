/**
 * Created by felipesaez on 6/04/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ConsentsList from './ConsentsList';
import { selectConsent } from 'actions/actions';
import { getMappedItemsSelector } from 'reducers/selectors';

class ConsentsListContainer extends React.Component {
  render() {
    return (
      <ConsentsList
        onChange={ this.props.selectConsent }
        mappedConsents={ this.props.mappedConsents }/>
    );
  }
}

const mapStateToProps = (state) => ({
  mappedConsents: getMappedItemsSelector(state)
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({ selectConsent }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ConsentsListContainer);