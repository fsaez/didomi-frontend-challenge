/**
 * Created by felipesaez on 6/04/18.
 */
import React from 'react';
import Checkbox from 'components/checkbox';
import { Wrapper } from './styles';

const ConsentsList = (props) => (
  <Wrapper>
    {
      props.mappedConsents.map(consent => (
          <Checkbox
            key={ consent.id }
            id={ consent.id }
            text={ consent.text }
            checked={ consent.checked }
            index={ consent.index }
            onChange={ props.onChange } />
        )
      )
    }
  </Wrapper>
);

export default ConsentsList;