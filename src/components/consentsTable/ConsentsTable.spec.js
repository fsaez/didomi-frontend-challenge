/**
 * Created by felipesaez on 9/04/18.
 */
import Enzyme, { shallow } from 'enzyme';
import ConsentsTable from './ConsentsTable';
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe('ConsentsTable', () => {

  it('renders properly', () => {
    const props = {
      usersDataSubset: [
        {
          name: 'John',
          email: 'john@test.com',
          consents: [ 'Receive newsletter' ]
        },
        {
          name: 'Alice',
          email: 'alice@test.com',
          consents: [ 'Receive newsletter', 'Be shown targeted ads' ]
        }
      ]
    };

    const enzymeWrapper = shallow(<ConsentsTable {...props} />);

    const rowColumnElements1 = enzymeWrapper.find('TableBody TableRow').at(0).find('TableRowColumn');
    const rowColumnElements2 = enzymeWrapper.find('TableBody TableRow').at(1).find('TableRowColumn');

    const nameCell1 = rowColumnElements1.get(0).props.children;
    const emailCell1 = rowColumnElements1.get(1).props.children;
    const consentsCell1 = rowColumnElements1.get(2).props.children;

    expect(nameCell1).toEqual('John');
    expect(emailCell1).toEqual('john@test.com');
    expect(consentsCell1).toEqual('Receive newsletter');

    const nameCell2 = rowColumnElements2.get(0).props.children;
    const emailCell2 = rowColumnElements2.get(1).props.children;
    const consentsCell2 = rowColumnElements2.get(2).props.children;

    expect(nameCell2).toEqual('Alice');
    expect(emailCell2).toEqual('alice@test.com');
    expect(consentsCell2).toEqual('Receive newsletter, Be shown targeted ads');
  });
});