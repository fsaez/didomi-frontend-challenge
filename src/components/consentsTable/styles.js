/**
 * Created by felipesaez on 9/04/18.
 */
import styled from 'styled-components';

export const consentsInlineStyle = {
  whiteSpace: 'normal'
};

export const Wrapper = styled.div`
  height: 100%;
  position: relative;
`;

export const IndexBarWrapper = styled.div`
  position: absolute;
  bottom: 20px;
  width: 100%;
`;