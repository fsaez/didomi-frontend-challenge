/**
 * Created by felipesaez on 6/04/18.
 */
import React from 'react';
import IndexBar from 'components/indexBar';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import { consentsInlineStyle, Wrapper, IndexBarWrapper } from './styles';

const ConsentsTable = (props) => (
  <Wrapper>
    <Table selectable={ false } multiSelectable={ false }>
      <TableHeader adjustForCheckbox={ false } displaySelectAll={ false }>
        <TableRow>
          <TableHeaderColumn>Name</TableHeaderColumn>
          <TableHeaderColumn>Email</TableHeaderColumn>
          <TableHeaderColumn>Consent given for</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={ false }>
      { props.usersDataSubset.map((user, index) =>
        <TableRow key={ index }>
          <TableRowColumn>{ user.name }</TableRowColumn>
          <TableRowColumn>{ user.email }</TableRowColumn>
          <TableRowColumn style={ consentsInlineStyle }>{ user.consents.map(consent => consent).join(', ') }</TableRowColumn>
        </TableRow>) }
      </TableBody>
    </Table>
    <IndexBarWrapper>
      <IndexBar/>
    </IndexBarWrapper>
  </Wrapper>
);

export default ConsentsTable;