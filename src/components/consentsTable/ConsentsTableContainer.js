/**
 * Created by felipesaez on 6/04/18.
 */
import React from 'react';
import ConsentsTable from './ConsentsTable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadConsents } from 'actions/actions';
import { getDataSubset } from 'reducers/selectors';

class ConsentsTableContainer extends React.Component {
  componentWillMount() {
    this.props.loadConsents();
  }
  render() {
    return <ConsentsTable usersDataSubset={ this.props.usersDataSubset } />;
  }
}

const mapStateToProps = (state) => ({
  usersDataSubset: getDataSubset(state)
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({ loadConsents }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ConsentsTableContainer);