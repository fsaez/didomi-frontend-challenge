/**
 * Created by felipesaez on 9/04/18.
 */
import styled from 'styled-components';
import Paper from 'material-ui/Paper';
import { NavLink } from 'react-router-dom';

export const StyledPaper = styled(Paper)`
  height: 100%;
`;

export const StyledNavLink = styled(NavLink)`
  color: black;
  text-decoration: none;
`;