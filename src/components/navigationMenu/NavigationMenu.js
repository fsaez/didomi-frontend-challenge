/**
 * Created by felipesaez on 7/04/18.
 */
import React from 'react';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import { StyledPaper, StyledNavLink } from './styles';

const NavigationMenu = () => (
  <div>
    <StyledPaper>
      <Menu>
        <MenuItem><StyledNavLink to="/give-consent">Give consent</StyledNavLink></MenuItem>
        <MenuItem><StyledNavLink to="/consents">Collected consents</StyledNavLink></MenuItem>
      </Menu>
    </StyledPaper>
  </div>
);

export default NavigationMenu;