/**
 * Created by felipesaez on 6/04/18.
 */
import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import styled from 'styled-components';
import NumberButtonsList from './numberButtonsList';

const numberButtonInlineStyle = {
  minWidth: 40
};

const buttonInlineStyle = {
  minWidth: 150
};

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-end;
`;

const IndexBar = (props) => (
  <Wrapper>
    <RaisedButton
      id="previous-button"
      style={ buttonInlineStyle }
      onClick={ e => props.navigatePrevious(props.currentIndex) }>Previous page</RaisedButton>
    <NumberButtonsList
      goToIndex={ props.goToIndex }
      currentIndex={ props.currentIndex }
      style={ numberButtonInlineStyle }
      indexesData={ props.indexesData }/>
    <RaisedButton
      id="next-button"
      style={ buttonInlineStyle }
      onClick={ e => props.navigateNext(props.currentIndex, props.maxIndex) }>Next page</RaisedButton>
  </Wrapper>

);

export default IndexBar;