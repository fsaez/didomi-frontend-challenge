/**
 * Created by felipesaez on 9/04/18.
 */
import Enzyme, { shallow } from 'enzyme';
import NumberButtonsList from './NumberButtonsList';
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe('NumberButtonsList', () => {

  it('renders properly', () => {
    const props = {
      maxIndex: 2,
      currentIndex: 0,
      indexesData: [ 0, 1, 2 ],
      totalIndexes: 3
    };

    const enzymeWrapper = shallow(<NumberButtonsList {...props} />);

    const numberButtons = enzymeWrapper.find('RaisedButton');

    const buttonLabel1 = numberButtons.get(0).props.children;
    const buttonLabel2 = numberButtons.get(1).props.children;
    const buttonLabel3 = numberButtons.get(2).props.children;

    expect(buttonLabel1).toBe(1);
    expect(buttonLabel2).toBe(2);
    expect(buttonLabel3).toBe(3);

    expect(numberButtons).toHaveLength(3);
  });
});