/**
 * Created by felipesaez on 9/04/18.
 */
import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const NumberButtonsList = (props) => (
  <div>
    {
      props.indexesData.map(index => {
        const isSelected = index === props.currentIndex;
        return <RaisedButton
          style={ props.style }
          primary={ isSelected }
          key={ index }
          onClick={ e => props.goToIndex(index) }>{ index + 1 }</RaisedButton>
      })
    }
  </div>
);

export default NumberButtonsList;