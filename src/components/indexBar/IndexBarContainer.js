/**
 * Created by felipesaez on 6/04/18.
 */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import IndexBar from './IndexBar';
import { getIndexesDataSelector, getMaxIndexSelector, getTotalIndexesSelector } from 'reducers/selectors';
import { goToIndex, navigatePrevious, navigateNext } from 'actions/actions';

class IndexBarContainer extends React.Component {
  render() {
    return <IndexBar
      indexesData={ this.props.indexesData }
      maxIndex={ this.props.maxIndex }
      totalIndexes={ this.props.totalIndexes }
      currentIndex={ this.props.currentIndex }
      goToIndex={ this.props.goToIndex }
      navigatePrevious={ this.props.navigatePrevious }
      navigateNext={ this.props.navigateNext }/>
  }
}

const mapStateToProps = (state) => ({
  maxIndex: getMaxIndexSelector(state),
  currentIndex: state.usersData.currentIndex,
  indexesData: getIndexesDataSelector(state),
  totalIndexes: getTotalIndexesSelector(state)
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({ goToIndex, navigatePrevious, navigateNext }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(IndexBarContainer);