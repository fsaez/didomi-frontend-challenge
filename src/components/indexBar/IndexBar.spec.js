/**
 * Created by felipesaez on 9/04/18.
 */
import Enzyme, { shallow } from 'enzyme';
import IndexBar from './IndexBar';
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe('IndexBar', () => {

  it('renders properly', () => {
    const props = {
      maxIndex: 2,
      currentIndex: 0,
      indexesData: [ 0, 1, 2 ],
      totalIndexes: 3
    };

    const enzymeWrapper = shallow(<IndexBar {...props} />);

    const previousButtonLabel = enzymeWrapper.find({ id: 'previous-button' }).props().children;
    expect(previousButtonLabel).toBe('Previous page');

    const nextButtonLabel = enzymeWrapper.find({ id: 'next-button' }).props().children;
    expect(nextButtonLabel).toBe('Next page');
  });
});