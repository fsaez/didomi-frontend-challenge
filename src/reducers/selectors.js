/**
 * Created by felipesaez on 7/04/18.
 */
import { createSelector } from 'reselect';

//Number used to set the group size in the consents table
const groupSize = 2;

const usersDataSelector = state => state.usersData.items;

//Selector used to get the number of indexes
export const getTotalIndexesSelector= createSelector(
  usersDataSelector,
  usersData => Math.ceil(usersData.length / groupSize)
);

//Selector used to get the max index when user navigates through the table of consent pages
export const getMaxIndexSelector= createSelector(
  getTotalIndexesSelector,
  totalIndexes => totalIndexes - 1
);

//Selector used to get an array with indexes based on the number of
// consents loaded and the size of the group to be displayed
export const getIndexesDataSelector = createSelector(
  getTotalIndexesSelector,
  totalIndexes => {
    let array = [];
    for (let i = 0; i < totalIndexes; i++) {
      array.push(i);
    }
    return array;
  }
);

const consentsAvailableSelector = state => state.form.consentsAvailable;
const indexesSelectedSelector = state => state.form.indexesSelected;

//Selector that based on a list of objects with info related to consents, gets the data required to display a list of checkboxes,
//depending on whether the consent is already in the selected indexes or not
export const getMappedItemsSelector = createSelector(
  [ consentsAvailableSelector, indexesSelectedSelector ],
  (consentsAvailable, indexesSelected) => {
    return consentsAvailable.map((consent) => {
      return indexesSelected.includes(consent.index) ? { ...consent, checked: true } : { ...consent, checked: false };
    });
  }
);

//Selector used to filter only the consents that have been selected
export const getSelectedItemsSelector = createSelector(
  [ consentsAvailableSelector, indexesSelectedSelector ],
  (consentsAvailable, indexesSelected) => {
    return consentsAvailable.filter((consent) => indexesSelected.includes(consent.index));
  }
);

const currentIndexSelector = state => state.usersData.currentIndex;

//Selector used to get a subset of the consents loaded from the remote so they can be displayed
export const getDataSubset = createSelector(
  [ usersDataSelector, currentIndexSelector ],
  (usersData, currentIndex) => {
    const startIndex = currentIndex * groupSize;
    const endIndex = startIndex + groupSize;

    return usersData.slice(startIndex, endIndex);
  }
);