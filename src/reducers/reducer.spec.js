/**
 * Created by felipesaez on 8/04/18.
 */
import reducer from './reducer';
import * as actions from 'actions/actions';
import * as actionTypes from 'actions/actionTypes';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as constants from 'constants/constants';

const middlewares = [ thunk ];
const mockStore = configureMockStore(middlewares);

describe('reducers', () => {

  it('Update email address using invalid value', () => {
    const expectedActions = [
      { type: actionTypes.SANITISE_EMAIL_ADDRESS,
        emailAddress: 'invalid'
      },
      { type: actionTypes.NOTIFY_EMAIL_IS_INVALID },
      { type: actionTypes.NOTIFY_FORM_IS_INVALID }
    ];

    const store = mockStore(constants.initialState);

    store.dispatch(actions.updateEmailAddress('invalid'));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('Update email address using valid value', () => {
    const expectedActions = [
      { type: actionTypes.SANITISE_EMAIL_ADDRESS,
        emailAddress: 'a@test.com'
      },
      { type: actionTypes.NOTIFY_EMAIL_IS_VALID },
      { type: actionTypes.NOTIFY_FORM_IS_INVALID }
    ];

    const store = mockStore(constants.initialState);

    store.dispatch(actions.updateEmailAddress('a@test.com'));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('Adds selected index', () => {

    const currentState = {
      form: {
        indexesSelected: []
      },
      usersData: {}
    };

    const expectedState1 = {
      form: {
        indexesSelected: [1]
      },
      usersData: {}
    };

    const expectedState2 = {
      form: {
        indexesSelected: [1, 2]
      },
      usersData: {}
    };

    const actualState = reducer(currentState, actions.notifyConsentHasBeenSelected(1));
    const actualState2 = reducer(actualState, actions.notifyConsentHasBeenSelected(2));

    expect(actualState).toEqual(expectedState1);
    expect(actualState2).toEqual(expectedState2);
  });

  it('Remove selected index', () => {

    const currentState = {
      form: {
        indexesSelected: [1]
      },
      usersData: {}
    };

    const expectedState = {
      form: {
        indexesSelected: []
      },
      usersData: {}
    };

    //since the index is already added, selecting again (which simulates a click on a selected checkbox)
    //will remove it from the array
    const actualState = reducer(currentState, actions.notifyConsentHasBeenSelected(1));

    expect(actualState).toEqual(expectedState);
  });

  it('resets form correctly', () => {

    const currentState = {
      form: {
        isSubmitting: false,
        isValid: true,
        success: false,
        name: {
          text: 'Juan'
        },
        email: {
          text: 'test@test.com',
          errorText: ''
        },
        indexesSelected: [],
      },
      usersData: {}
    };

    const expectedState = {
      form: {
        isSubmitting: false,
        isValid: false,
        success: false,
        name: {
          text: ''
        },
        email: {
          text: '',
          errorText: ''
        },
        indexesSelected: [],
      },
      usersData: {}
    };

    //since the index is already added, selecting again (which simulates a click on a selected checkbox)
    //will remove it from the array
    const actualState = reducer(currentState, actions.resetForm());

    expect(actualState).toEqual(expectedState);
  });

  it('Goes to index', () => {
    const currentState = {
      form: {
        indexesSelected: []
      },
      usersData: {}
    };

    const expectedState = {
      form: {
        indexesSelected: []
      },
      usersData: {
        currentIndex: 1
      }
    };

    const actualState = reducer(currentState, actions.goToIndex(1));

    expect(actualState).toEqual(expectedState);
  });
});