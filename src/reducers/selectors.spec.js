/**
 * Created by felipesaez on 9/04/18.
 */
import {
  getTotalIndexesSelector,
  getMaxIndexSelector,
  getMappedItemsSelector,
  getSelectedItemsSelector,
  getDataSubset,
  getIndexesDataSelector
} from 'reducers/selectors';

describe('selectors', () => {
  it('gets getTotalIndexesSelector properly for groupSize = 2 and 3 items', () => {

    const currentState = {
      usersData: {
        items: [{}, {}, {}]
      }
    };

    const actualTotalIndexes = getTotalIndexesSelector(currentState);
    const expectedTotalIndexes = 2;

    expect(actualTotalIndexes).toEqual(expectedTotalIndexes);
  });

  it('gets getTotalIndexesSelector properly for groupSize = 2 and 6 items', () => {

    const currentState = {
      usersData: {
        items: [{}, {}, {}, {}, {}, {}]
      }
    };

    const actualTotalIndexes = getTotalIndexesSelector(currentState);
    const expectedTotalIndexes = 3;

    expect(actualTotalIndexes).toEqual(expectedTotalIndexes);
  });

  it('gets getTotalIndexesSelector properly for groupSize = 2 and 1 item', () => {

    const currentState = {
      usersData: {
        items: [{}]
      }
    };

    const actualTotalIndexes = getTotalIndexesSelector(currentState);
    const expectedTotalIndexes = 1;

    expect(actualTotalIndexes).toEqual(expectedTotalIndexes);
  });

  it('gets getMaxIndexSelector properly for groupSize 2 and 3 items', () => {
    const currentState = {
      usersData: {
        items: [{}, {}, {}]
      }
    };

    const actualMaxIndex = getMaxIndexSelector(currentState);
    const expectedMaxIndex = 1;

    expect(actualMaxIndex).toEqual(expectedMaxIndex);
  });

  it('gets getMaxIndexSelector properly for groupSize 2 and 6 items', () => {
    const currentState = {
      usersData: {
        items: [{}, {}, {}, {}, {}, {}]
      }
    };

    const actualMaxIndex = getMaxIndexSelector(currentState);
    const expectedMaxIndex = 2;

    expect(actualMaxIndex).toEqual(expectedMaxIndex);
  });

  it('gets getMaxIndexSelector properly for groupSize 2 and 6 items', () => {
    const currentState = {
      usersData: {
        items: [{}]
      }
    };

    const actualMaxIndex = getMaxIndexSelector(currentState);
    const expectedMaxIndex = 0;

    expect(actualMaxIndex).toEqual(expectedMaxIndex);
  });

  it('mapps items properly', () => {
    const currentState = {
      form: {
        indexesSelected: [0, 1],
        consentsAvailable: [{
          index: 0
        }, {
          index: 1
        }, {
          index: 2
        }]
      }
    };

    const actualMappedItems = getMappedItemsSelector(currentState);
    const expectedItems = [
      {
        index: 0,
        checked: true
      },
      {
        index: 1,
        checked: true
      },
      {
        index: 2,
        checked: false
      }
    ];

    expect(actualMappedItems).toEqual(expectedItems);
  });

  it('gets checked items properly', () => {
    const currentState = {
      form: {
        indexesSelected: [0, 1],
        consentsAvailable: [{
          index: 0
        }, {
          index: 1
        }, {
          index: 2
        }]
      }
    };

    const actualItems = getSelectedItemsSelector(currentState);
    const expectedItems = [{
      index: 0
    }, {
      index: 1
    }];

    expect(actualItems).toEqual(expectedItems);
  });

  it('gets data subset properly. First group', () => {
    const currentState = {
      usersData: {
        currentIndex: 0,
        items: [{
          index: 0
        }, {
          index: 1
        }, {
          index: 2
        }, {
          index: 3
        }]
      }
    };

    const actualDataSubset = getDataSubset(currentState);
    const expectedDataSubset = [{
      index: 0
    }, {
      index: 1
    }];

    expect(actualDataSubset).toEqual(expectedDataSubset);
  });

  it('gets data subset properly. Second group', () => {
    const currentState = {
      usersData: {
        currentIndex: 1,
        items: [{
          index: 0
        }, {
          index: 1
        }, {
          index: 2
        }, {
          index: 3
        }]
      }
    };

    const actualDataSubset = getDataSubset(currentState);
    const expectedDataSubset = [{
      index: 2
    }, {
      index: 3
    }];

    expect(actualDataSubset).toEqual(expectedDataSubset);
  });

  it('calculates indexes data correctly', () => {
    const currentState = {
      usersData: {
        items: [{
          index: 0
        }, {
          index: 1
        }, {
          index: 2
        }, {
          index: 3
        }, {
          index: 4
        }, {
          index: 5
        }, {
          index: 6
        }, {
          index: 7
        }]
      }
    };
    const actualIndexesData = getIndexesDataSelector(currentState);
    const expectedIndexesData = [0, 1, 2, 3];

    expect(actualIndexesData).toEqual(expectedIndexesData);
  });
});