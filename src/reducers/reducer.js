/**
 * Created by felipesaez on 5/04/18.
 */
import * as actionTypes from 'actions/actionTypes';
import * as constants from 'constants/constants';
import { combineReducers } from 'redux';

const form = (state = constants.initialState.form, action) => {
  switch (action.type) {
    case actionTypes.SANITISE_NAME:
      return { ...state, name: { ...state.name, text: action.name } };
    case actionTypes.SANITISE_EMAIL_ADDRESS:
      return { ...state, email: { ...state.email, text: action.emailAddress } };
    case actionTypes.NOTIFY_EMAIL_IS_INVALID:
      return { ...state, email: { ...state.email, errorText: 'Invalid email' } };
    case actionTypes.NOTIFY_EMAIL_IS_VALID:
      return { ...state, email: { ...state.email, errorText: null } };
    case actionTypes.NOTIFY_CONSENT_HAS_BEEN_SELECTED:
      const indexesSelected = state.indexesSelected;
      //action.index doesn't necessarily represent the position on the array so we need to get it
      const position = indexesSelected.indexOf(action.index);
      return {
        ...state, indexesSelected: indexesSelected.includes(action.index) ?
          [ ...indexesSelected.slice(0, position), ...indexesSelected.slice(position + 1) ] :
          [ ...indexesSelected, action.index ]
      };
    case actionTypes.NOTIFY_FORM_IS_VALID:
      return { ...state, isValid: true };
    case actionTypes.NOTIFY_FORM_IS_INVALID:
      return { ...state, isValid: false };
    case actionTypes.REQUEST_TO_SUBMIT_FORM:
      return { ...state, isSubmitting: action.isSubmitting };
    case actionTypes.NOTIFY_THAT_CONSENT_WAS_RECEIVED:
      return { ...state, success: action.success };
    case actionTypes.RESET_FORM:
      return {
        ...state,
        isSubmitting: false,
        isValid: false,
        success: false,
        name: {
          text: ''
        },
        email: {
          text: '',
          errorText: ''
        },
        indexesSelected: [],
      };
    default:
      return state;
  }
};

const usersData = (state = constants.initialState.usersData, action) => {
  switch (action.type) {
    case actionTypes.REQUEST_TO_LOAD_CONSENTS:
      return { ...state, isLoading: true }
    case actionTypes.NOTIFY_CONSENTS_HAVE_BEEN_LOADED:
      return { ...state, items: action.usersData, isLoading: false };
    case actionTypes.GO_TO_INDEX:
      return { ...state, currentIndex: action.currentIndex };
    default:
      return state;
  }
};

const reactApp = combineReducers({
  form,
  usersData
});

export default reactApp;