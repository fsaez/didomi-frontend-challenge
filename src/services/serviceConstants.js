const BASE_URL = 'http://localhost:9001';
const GET_CONSENTS_URL = BASE_URL + '/consents';
const POST_CONSENTS_URL = BASE_URL + '/consents';

export default {
  BASE_URL,
  GET_CONSENTS_URL,
  POST_CONSENTS_URL
};