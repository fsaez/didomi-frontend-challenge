/**
 * Method used to fetch data from a remote service
 *
 * @param options {Object} properties required to call the fetch method
 * @param options.url {String} URL used to call the service
 * @param options.success {Function} method called when call is successful
 * @returns {Promise} promise from fetch method
 */
const baseFetch = (options) => {
  return fetch(options.url).then((response) => {
    return response.json();
  }).then((result) => {
    options.success(result);
  });
};

/**
 * Method used to post data to a remote service
 *
 * @param options {Object} properties required to call the fetch method
 * @param options.url {String} URL used to call the service
 * @param options.success {Function} method called when call is successful
 * @param options.payload {Object} properties to send to the remote service
 * @returns {Promise} promise from fetch method
 */
const basePost = (options) => {
  return fetch(options.url, {
    method: 'POST',
    body: JSON.stringify(options.payload),
    headers: {
      'Content-Type': 'application/json',
    }
  }).then((response) => {
    return response.json();
  }).then((result) => {
    options.success(result)
  }).catch((info) => {
    options.error(info)
  });
};

const throwParametersError = () => {
  throw new Error('Parameters have not been provided properly');
};

export default {
  fetch: baseFetch,
  throwParametersError: throwParametersError,
  basePost: basePost
};