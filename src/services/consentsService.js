import BaseServiceProvider from './baseServiceProvider';
import ServiceConstants from './serviceConstants';

const fetchAll = (options) => {
  options.url = ServiceConstants.GET_CONSENTS_URL;
  return BaseServiceProvider.fetch(options);
};

const postConsent = (options) => {
  options.url = ServiceConstants.POST_CONSENTS_URL;
  return BaseServiceProvider.basePost(options);
};

export default {
  fetchAll,
  postConsent
};