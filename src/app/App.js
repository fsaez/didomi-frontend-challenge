import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import thunk from 'redux-thunk';
import reducer from 'reducers/reducer';
import ConsentCollectionPage from 'scenes/consentCollectionPage';
import ConsentManagementPage from 'scenes/consentManagementPage';
import NavigationMenu from 'components/navigationMenu';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import styled from 'styled-components';

const FlexboxWrapper = styled.div`
  display: flex;
  align-items: stretch;
  height: 400px;
  margin: auto;
`;

const Content = styled.div`
  margin-left: 5px;
  flex-grow: 1
`;

const store = createStore(
  reducer,
  applyMiddleware(thunk)
);

const App = () => (
  <Provider store={ store }>
    <Router>
      <MuiThemeProvider>
        <div>
          <AppBar
            title="Consent management"
            showMenuIconButton={ false }/>
          <FlexboxWrapper>
            <Route path='/' component={ NavigationMenu }/>
            <Content>
              <Switch>
                <Redirect exact={true} from="/" to="/give-consent"/>
                <Route exact path='/give-consent' component={ ConsentCollectionPage }/>
                <Route exact path='/consents' component={ ConsentManagementPage }/>
              </Switch>
            </Content>
          </FlexboxWrapper>
        </div>
      </MuiThemeProvider>
    </Router>
  </Provider>
);

export default App;
