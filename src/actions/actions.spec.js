/**
 * Created by felipesaez on 5/04/18.
 */
import * as actionTypes from './actionTypes';
import * as actions from './actions';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions.js', () => {
  describe('actions creation', () => {
    it('creates SANITISE_NAME action', () => {
      const expectedAction = {
        type: actionTypes.SANITISE_NAME,
        name: 'Peter'
      };

      const actualAction = actions.sanitiseName('Peter');

      expect(actualAction).toEqual(expectedAction);
    });

    it('creates SANITISE_EMAIL_ADDRESS action', () => {
      const expectedAction = {
        type: actionTypes.SANITISE_EMAIL_ADDRESS,
        emailAddress: 'test@test.com'
      };

      const actualAction = actions.sanitiseEmailAddress('test@test.com');

      expect(actualAction).toEqual(expectedAction);
    });

    it('creates SANITISE_NAME action removing invalid characters', () => {
      const expectedAction = {
        type: actionTypes.SANITISE_NAME,
        name: 'Peter'
      };

      const actualAction = actions.sanitiseName('Peter*?(');

      expect(actualAction).toEqual(expectedAction);
    });

    it('creates SANITISE_EMAIL_ADDRESS action removing invalid characters', () => {
      const expectedAction = {
        type: actionTypes.SANITISE_EMAIL_ADDRESS,
        emailAddress: 'Peter@test.com'
      };

      const actualAction = actions.sanitiseEmailAddress('Peter@test.com * ? (');

      expect(actualAction).toEqual(expectedAction);
    });

    it('creates NOTIFY_CONSENT_HAS_BEEN_SELECTED action', () => {
      const expectedAction = {
        type: actionTypes.NOTIFY_CONSENT_HAS_BEEN_SELECTED,
        index: 1
      };

      const actualAction = actions.notifyConsentHasBeenSelected(1);

      expect(actualAction).toEqual(expectedAction);
    });

    it('creates REQUEST_TO_SUBMIT_FORM action', () => {
      const expectedAction = {
        type: actionTypes.REQUEST_TO_SUBMIT_FORM
      };

      const actualAction = actions.requestToSubmitForm();

      expect(actualAction).toEqual(expectedAction);
    });

    it('creates NOTIFY_THAT_CONSENT_WAS_RECEIVED action', () => {
      const expectedAction = {
        type: actionTypes.NOTIFY_THAT_CONSENT_WAS_RECEIVED
      };

      const actualAction = actions.notifyThatConsentWasReceived();

      expect(actualAction).toEqual(expectedAction);
    });
  });
  
  describe('actions dispatching', () => {
    it('dispatches SANITISE_NAME and NOTIFY_FORM_IS_INVALID actions when name is entered and other fields are not', () => {
      const currentState = {
        form: {
          name: {
            text: 'a'
          },
          email: {
            text: '',
            errorText: ''
          },
          indexesSelected: []
        }
      };
      const expectedActions = [
        {
          type: actionTypes.SANITISE_NAME,
          name: 'a'
        },
        {
          type: actionTypes.NOTIFY_FORM_IS_INVALID
        }
      ];

      const store = mockStore(currentState);
      store.dispatch(actions.updateName('a'));

      expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches SANITISE_NAME and NOTIFY_FORM_IS_VALID actions when all fields are entered correctly and updateName is dispatched', () => {
      const currentState = {
        form: {
          name: {
            text: 'a'
          },
          email: {
            text: 'a@test.com'
          },
          indexesSelected: [1]
        }
      };
      const expectedActions = [
        {
          type: actionTypes.SANITISE_NAME,
          name: 'a'
        },
        {
          type: actionTypes.NOTIFY_FORM_IS_VALID
        }
      ];

      const store = mockStore(currentState);
      store.dispatch(actions.updateName('a'));

      expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches SANITISE_NAME and NOTIFY_FORM_IS_VALID actions when all fields are entered correctly and updateEmailAddress is dispatched', () => {
      const currentState = {
        form: {
          name: {
            text: 'a'
          },
          email: {
            text: 'a@test.com'
          },
          indexesSelected: [1]
        }
      };
      const expectedActions = [
        {
          type: actionTypes.SANITISE_EMAIL_ADDRESS,
          emailAddress: 'a@test.com'
        },
        {
          type: actionTypes.NOTIFY_EMAIL_IS_VALID
        },
        {
          type: actionTypes.NOTIFY_FORM_IS_VALID
        }
      ];

      const store = mockStore(currentState);
      store.dispatch(actions.updateEmailAddress('a@test.com'));

      expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches goToIndex thunk with the right parameters when navigatePrevious is called', () => {
      const currentState = {
        usersData: {
          currentIndex: 1
        }
      };

      const expectedActions = [
        {
          type: actionTypes.GO_TO_INDEX,
          currentIndex: 2
        }
      ];

      const store = mockStore(currentState);
      const maxIndex = 2; // this is calculated by getMaxIndexSelector
      store.dispatch(actions.navigateNext(currentState.usersData.currentIndex, maxIndex));

      expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches goToIndex thunk with the right parameters when navigatePrevious is called when at the end', () => {
      const currentState = {
        usersData: {
          currentIndex: 2
        }
      };

      const expectedActions = [
        {
          type: actionTypes.GO_TO_INDEX,
          currentIndex: 2
        }
      ];

      const store = mockStore(currentState);
      const maxIndex = 2; // this is calculated by getMaxIndexSelector
      store.dispatch(actions.navigateNext(currentState.usersData.currentIndex, maxIndex));

      expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches goToIndex thunk with the right parameters when navigatePrevious is called when at the beginning', () => {
      const currentState = {
        usersData: {
          currentIndex: 0
        }
      };

      const expectedActions = [
        {
          type: actionTypes.GO_TO_INDEX,
          currentIndex: 0
        }
      ];

      const store = mockStore(currentState);
      store.dispatch(actions.navigatePrevious(currentState.usersData.currentIndex));

      expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches goToIndex thunk with the right parameters when navigatePrevious is called when', () => {
      const currentState = {
        usersData: {
          currentIndex: 1
        }
      };

      const expectedActions = [
        {
          type: actionTypes.GO_TO_INDEX,
          currentIndex: 0
        }
      ];

      const store = mockStore(currentState);
      store.dispatch(actions.navigatePrevious(currentState.usersData.currentIndex));

      expect(store.getActions()).toEqual(expectedActions);
    });

    afterEach(() => {
      fetchMock.reset();
      fetchMock.restore();
    });

    it('dispatches REQUEST_TO_LOAD_CONSENTS and NOTIFY_CONSENTS_HAVE_BEEN_LOADED after loadConsents is called', (done) => {
      fetchMock
        .getOnce('http://localhost:9001/consents', { body: [{
          "name": "Felipe",
          "email": "felipe@test.com",
          "consents": [
            "Receive newsletter"
          ],
          "id": 1
        }]});

      const expectedActions = [{
          type: actionTypes.REQUEST_TO_LOAD_CONSENTS
        }, {
          type: actionTypes.NOTIFY_CONSENTS_HAVE_BEEN_LOADED,
          usersData: [{
            "name": "Felipe",
            "email": "felipe@test.com",
            "consents": [
              "Receive newsletter"
            ],
            "id": 1
          }]
      }];

      const store = mockStore({});

      store.dispatch(actions.loadConsents()).then(() => {
        const actualActions = store.getActions();
        expect(actualActions).toEqual(expectedActions);
        done();
      });
    });

    it('dispatches REQUEST_TO_SUBMIT_FORM and NOTIFY_THAT_CONSENT_WAS_RECEIVED after submitForm is called', (done) => {
      fetchMock
        .postOnce('http://localhost:9001/consents', { body: {}});

      const expectedActions = [{
        type: actionTypes.REQUEST_TO_SUBMIT_FORM
      }, {
        type: actionTypes.NOTIFY_THAT_CONSENT_WAS_RECEIVED,
        success: true
      }];

      const store = mockStore({});

      store.dispatch(actions.submitForm(true, {})).then(() => {
        const actualActions = store.getActions();
        expect(actualActions).toEqual(expectedActions);
        done();
      });
    });
  });
});