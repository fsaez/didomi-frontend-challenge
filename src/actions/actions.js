/**
 * Created by felipesaez on 5/04/18.
 */
import * as actionTypes from './actionTypes';
import consentsService from 'services/consentsService';

const nameCharactersRegex = /([^a-z ]+)/gi;
const emailCharactersRegex = /([^a-z@.]+)/gi;
const emailFormatRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const loadConsents = () => {
  return (dispatch) => {
    dispatch(requestToLoadConsents());
    return consentsService.fetchAll({
      success(payload) {
        dispatch(notifyConsentsHaveBeenLoaded(payload));
      }
    });
  };
};

export const requestToLoadConsents = () => ({
  type: actionTypes.REQUEST_TO_LOAD_CONSENTS
});

export const notifyConsentsHaveBeenLoaded = (usersData) => ({
  type: actionTypes.NOTIFY_CONSENTS_HAVE_BEEN_LOADED,
  usersData: usersData
});

export const updateName = (name) => {
  return (dispatch) => {
    dispatch(sanitiseName(name));
    dispatch(validateForm());
  };
};

export const sanitiseName = (name) => ({
  type: actionTypes.SANITISE_NAME,
  name: name.replace(nameCharactersRegex, '')
});

export const updateEmailAddress = (emailAddress) => {
  return (dispatch) => {
    const isEmailValid = emailFormatRegex.test(emailAddress.toLowerCase()) || !emailAddress;
    dispatch(sanitiseEmailAddress(emailAddress));
    isEmailValid ? dispatch(notifyEmailIsValid()) : dispatch(notifyEmailIsInvalid());

    dispatch(validateForm());
  };
};

export const sanitiseEmailAddress = (emailAddress) => ({
  type: actionTypes.SANITISE_EMAIL_ADDRESS,
  emailAddress: emailAddress.replace(emailCharactersRegex, '')
});

export const notifyEmailIsValid = () => ({
  type: actionTypes.NOTIFY_EMAIL_IS_VALID
});

export const notifyEmailIsInvalid = () => ({
  type: actionTypes.NOTIFY_EMAIL_IS_INVALID
});

//The same action is triggered when an item from the list is checked or unchecked
export const selectConsent = (index) => {
  return (dispatch) => {
    dispatch(notifyConsentHasBeenSelected(index));
    dispatch(validateForm());
  };
};

export const notifyConsentHasBeenSelected = (index) => ({
  type: actionTypes.NOTIFY_CONSENT_HAS_BEEN_SELECTED,
  index
});

export const validateForm = () => {
  return (dispatch, getState) => {
    const currentState = getState();
    const isFormValid =
      currentState.form.indexesSelected.length > 0 &&
      currentState.form.name.text !== '' &&
      !currentState.form.email.errorText &&
      currentState.form.email.text !== '';

    isFormValid ? dispatch(notifyFormIsValid()) : dispatch(notifyFormIsInvalid());
  };
};

export const notifyFormIsValid = () => ({
  type: actionTypes.NOTIFY_FORM_IS_VALID
});

export const notifyFormIsInvalid = () => ({
  type: actionTypes.NOTIFY_FORM_IS_INVALID
});

export const resetForm = () => ({
  type: actionTypes.RESET_FORM
});

export const submitForm = (isValid, payload) => {
  return (dispatch) => {
    if (isValid) {
      dispatch(requestToSubmitForm());
      return consentsService.postConsent({
        payload: payload,
        success: () => {
          dispatch(notifyThatConsentWasReceived(true));
        }
      });
    }
  };
};

export const requestToSubmitForm = () => ({
  type: actionTypes.REQUEST_TO_SUBMIT_FORM
});

export const notifyThatConsentWasReceived = (success) => ({
  type: actionTypes.NOTIFY_THAT_CONSENT_WAS_RECEIVED,
  success
});

export const navigatePrevious = (current) => {
  return (dispatch) => {
    let currentIndex = current;

    if (currentIndex > 0) {
      currentIndex--;
    }
    dispatch(goToIndex(currentIndex));
  };
};

export const navigateNext = (current, maxIndex) => {
  return (dispatch) => {
    let currentIndex = current;

    if (currentIndex < maxIndex) {
      currentIndex++;
    }

    dispatch(goToIndex(currentIndex));
  };
};

export const goToIndex = (currentIndex) => ({
  type: actionTypes.GO_TO_INDEX,
  currentIndex
});