/**
 * Created by felipesaez on 7/04/18.
 */
export const consentsAvailable = [
  {
    id: 'newsletter-checkbox',
    index: 0,
    text: 'Receive newsletter',
  },
  {
    id: 'targeted-ads-checkbox',
    index: 1,
    text: 'Be shown targeted ads',
  },
  {
    id: 'visits-checkbox',
    index: 2,
    text: 'Contribute to anonymous visit statistics',
  }
];

export const initialState = {
  form: {
    isSubmitting: false,
    isValid: false,
    success: false,
    name: {
      text: ''
    },
    email: {
      text: '',
      errorText: ''
    },
    consentsAvailable: consentsAvailable, //hardcoded for the purpose of this code challenge
    indexesSelected: []
  },
  usersData: {
    isLoading: false,
    currentIndex: 0,
    items: []
  }
};